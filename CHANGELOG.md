## master (unreleased)

## [0.0.0] - 2024-07-02

* Breaking change: :arcgis_opts key renamed :arcrest_opts
