# frozen_string_literal: true

# scrapes feature layers
class FeatureScraper
  class Ogr2ogrVersionError < StandardError; end

  ESRIFIELDTYPEOID = 'esriFieldTypeOID'

  attr_reader :name

  def initialize(url:, arcrest_opts: {})
    @url = url
    @arcrest_opts = arcrest_opts # e.g. headers: { referer: '...' }
    @layer = layer
    @json = json
    @name = @layer.name
    @pk = pk
    @max_record_count = max_record_count
    @loops = loops
    @threads = GisScraper.config[:threads]
  end

  def json_data
    query_layer.merge('features' => all_features(@threads)).to_json
  end

  private

  def query_layer
    @layer.query(where: '1=1')
  end

  def layer
    ArcREST::Layer.new(@url, @arcrest_opts)
  end

  def json
    @layer.json
  end

  def renderer
    @layer.drawing_info['renderer']
  end

  def pk
    @json['fields'].select { |f| f['type'] == ESRIFIELDTYPEOID }[0]['name']
  end

  def max_record_count
    @layer.max_record_count
  end

  def count
    @layer.count
  end

  def features(num)
    @layer.features(where: where_text(num))
  end

  def all_features(threads)
    Parallel.map(0...@loops, in_threads: threads) { |n| features(n) }.flatten
  end

  def loops
    (count.to_f / @max_record_count).ceil
  end

  def where_text(num)
    num ? "#{pk} > #{num * @max_record_count} AND #{pk} <= #{(num + 1) * @max_record_count}" : "#{pk} > 0"
  end
end
