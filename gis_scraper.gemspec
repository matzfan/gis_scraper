# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'gis_scraper/version'

Gem::Specification.new do |spec|
  spec.name          = 'gis_scraper'
  spec.version       = GisScraper::VERSION
  spec.authors       = ['matzfan']

  spec.summary       = 'Utility to scrape ArcGIS data'
  spec.description   = 'Scrape ArcGIS data from REST API and export to postgres db'
  spec.homepage      = 'https://gitlab.com/matzfan/gis_scraper'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 3.3.3'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/matzfan/gis_scraper'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/matzfan/gis_scraper/CHANGELOG.md'
  spec.metadata = { 'rubygems_mfa_required' => 'true' }

  spec.files = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(spec)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'arcrest', '~> 1.1'
  spec.add_runtime_dependency 'parallel', '~> 1.25'
  spec.add_runtime_dependency 'pg', '~> 1.5'
end
