# frozen_string_literal: true

describe FeatureScraper do
  before { GisScraper.configure }

  server = 'https://sampleserver6.arcgisonline.com/ArcGIS/rest/services/'
  few_features_layer = "#{server}USA/MapServer/2" # US states
  many_features_layer = "#{server}USA/MapServer/3" # US counties

  let(:few_features_scraper) { described_class.new url: few_features_layer }
  let(:many_features_scraper) { described_class.new url: many_features_layer }
  let(:simple_renderer_scraper) { described_class.new url: many_features_layer }

  simple_renderer_hash = {
    'description' => '',
    'label' => '',
    'symbol' => {
      'type' => 'esriSFS',
      'style' => 'esriSFSSolid',
      'color' => [190, 210, 255, 255],
      'outline' => {
        'style' => 'esriSLSSolid',
        'type' => 'esriSLS',
        'color' => [110, 110, 110, 255],
        'width' => 0.4
      }
    },
    'type' => 'simple'
  }

  describe '#new(url)' do
    it 'instantiates an instance of the class' do
      expect(few_features_scraper.class).to eq described_class
    end
  end

  describe '#name' do
    it 'returns the name of the layer' do
      expect(few_features_scraper.name).to eq 'States'
    end
  end

  describe '#pk' do
    it 'returns the pk field, if it is first in the field list' do
      expect(few_features_scraper.send(:pk)).to eq 'objectid'
    end

    it 'returns the pk field, if it is elsewhere in the field list' do
      s = described_class.new url: 'http://gps.digimap.gg/arcgis/rest/services/JerseyUtilities/JerseyUtilities/MapServer/45'
      expect(s.send(:pk)).to eq 'OBJECTID'
    end
  end

  describe '#max_record_count' do
    it 'returns the layer query limit, if key: "maxRecordCount" exits' do
      expect(many_features_scraper.send(:max_record_count)).to eq 1000
    end
  end

  describe '#count' do
    it 'returns the number of records for the layer' do
      expect(many_features_scraper.send(:count)).to eq 3141
    end
  end

  describe '#loops' do
    it 'returns the number of loops needed to scraper the whole layer' do
      expect(many_features_scraper.send(:loops)).to eq 4
    end
  end

  describe '#all_features(num_threads)' do
    it 'returns an array of the features data for all layer objects' do
      many_features_scraper.instance_variable_set(:@max_record_count, 2)
      many_features_scraper.instance_variable_set(:@loops, 2)
      expect(many_features_scraper.send(:all_features, 1).count).to eq 4
    end
  end

  describe '#json_data', :public do
    it "returns string of json data including all of a layer's features if num features <= max_record_count" do
      expect(JSON.parse(few_features_scraper.json_data)['features'].size).to eq 51
    end

    it "returns string of json data including all of a layer's features if num features > max_record_count" do
      expect(JSON.parse(many_features_scraper.json_data)['features'].size).to eq 3141
    end
  end

  describe '#renderer', :public do
    it 'returns a hash-representation of the renderer' do
      expect(simple_renderer_scraper.send(:renderer)).to eq simple_renderer_hash
    end
  end

  describe '#query_layer' do
    fields = %w[displayFieldName fieldAliases geometryType spatialReference fields features exceededTransferLimit]
    it "returns a hash of a layer query whose value for the key 'features' has fields #{fields}" do
      hash = many_features_scraper.send(:query_layer)
      expect(hash.keys).to eq fields
    end

    it 'returns a hash of a layer query whose value for the key "features" has size equal to max_record_count' do
      hash = many_features_scraper.send(:query_layer)
      expect(hash['features'].size).to eq 1000 # max_record_count
    end
  end
end
