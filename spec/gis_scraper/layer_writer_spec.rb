# frozen_string_literal: true

describe LayerWriter do
  user = ENV.fetch('POSTGRES_USER', `whoami`.chomp)
  path = Dir.home
  file_name = 'Lamp_Posts.json' # layer ID 0
  sub_layer_ids = [30, 33, 36]
  tables = %w[_ducts _structure]
  server = 'https://gps.digimap.gg/arcgis/rest/services/'
  utilities = "#{server}JerseyUtilities/JerseyUtilities/MapServer/"

  let(:tmp) { Dir.mktmpdir }
  let(:dirs) do
    ["#{tmp}/Jersey Gas/High Pressure", "#{tmp}/Jersey Gas/Low Pressure", "#{tmp}/Jersey Gas/Medium Pressure"]
  end
  let(:feature_layer) { described_class.new(url: "#{utilities}0") }
  let(:group_layer) { described_class.new(url: "#{utilities}39") } # JT
  let(:layer_with_sub_grp_layers) { described_class.new(url: "#{utilities}29") } # Jersey Gas

  def conn
    PG.connect(host: ENV.fetch('PG_HOST', 'localhost'), # for gitlab CI
               dbname: ENV.fetch('DB', GisScraper.config[:dbname]),
               user: ENV.fetch('POSTGRES_USER', GisScraper.config[:user]))
  end

  def table_names
    conn.exec(LayerWriter::TABLES).map { |tup| tup['table_name'] }.sort
  end

  def clean_tmp_dir
    FileUtils.rm_rf("#{tmp}/*")
  end

  def clean_db_and_tmp_dir
    conn.exec 'drop schema public cascade;'
    conn.exec 'create schema public;'
    clean_tmp_dir
  end

  before do
    GisScraper.configure(output_path: tmp, srs: 'EPSG:3109', user: user)
    json = { fields: [{ name: 'OBJECTID', type: 'esriFieldTypeOID', alias: 'OBJECTID' }], features: [] }.to_json
    allow_any_instance_of(described_class).to receive(:json_data).and_return(json) # no FeatureScraper instantiated
  end

  describe '#new(url)' do
    it 'returns an instance of the class with a layer url string' do
      expect(feature_layer.class).to eq described_class
    end
  end

  describe '#validate_type' do
    it 'raises "Bad Layer type <layer_type>" if layer type is not in TYPES' do
      expect { feature_layer.send(:validate_layer, 'Unknown Layer') }.to raise_error 'Bad Layer type: Unknown Layer'
    end
  end

  describe '#type' do
    it 'returns the layer type for a feature layer' do
      expect(feature_layer.send(:type)).to eq 'Feature Layer'
    end

    it 'returns the layer type for a group layer' do
      expect(group_layer.send(:type)).to eq 'Group Layer'
    end
  end

  describe '#sub_layer_ids' do
    it 'returns an empty list for a feature layer (which have no sub layers)' do
      expect(feature_layer.send(:sub_layer_ids)).to eq []
    end

    it 'returns a list of the sublayer ids for a group layer, if any' do
      expect(layer_with_sub_grp_layers.send(:sub_layer_ids)).to eq sub_layer_ids
    end
  end

  describe '#write_json' do
    it 'writes a feature layer JSON to config path if no path is specified' do
      feature_layer.send :write_json
      expect(Dir["#{tmp}/*"]).to include "#{tmp}/#{file_name}"
    ensure
      clean_tmp_dir
    end

    it "writes a feature layer's data to a JSON file to the path specified" do
      described_class.new(url: "#{utilities}0", path: path).send :write_json
      expect(Dir["#{path}/*"]).to include "#{path}/#{file_name}"
    ensure
      `rm #{path}/#{Shellwords.escape(file_name)}`
    end
  end

  describe '#output_json', :public do
    it 'calls #write_json for a feature layer' do
      feature_layer.output_json
      expect(Dir["#{tmp}/*"]).to include "#{tmp}/#{file_name}"
    ensure
      clean_tmp_dir
    end

    context 'with a group layer' do
      it 'creates sub directories mirroring sub-group structure' do
        allow_any_instance_of(described_class).to receive :write_json
        layer_with_sub_grp_layers.output_json
        expect(dirs.all? { |d| Dir["#{tmp}/*/*"].include? d }).to be true
      ensure
        clean_tmp_dir
      end

      it 'calls #write_json_files for each underlying feature layer' do
        layer_with_sub_grp_layers.output_json
        dirs.all? { expect(Dir["#{tmp}/**/*.json"].size).to eq 6 } # total number of layers/json files
      ensure
        clean_tmp_dir
      end
    end
  end

  describe '#output_to_db' do
    it 'raises error if ogr2ogr executable is not found' do
      allow_any_instance_of(described_class).to receive(:ogr2ogr?).and_return nil
      err = 'ogr2ogr executable missing, is GDAL installed and in your PATH?'
      expect { feature_layer.output_to_db }.to raise_error err
    end

    it 'raises error if ogr2ogr version < 1.11.5' do
      allow_any_instance_of(described_class).to receive(:ogr2ogr?).and_return('GDAL 1.11.4, ')
      expect { feature_layer.output_to_db }.to raise_error 'ogr2ogr version must be > 1.11.4'
    end

    it 'writes a layer to a PostgresSQL db table with the name lowercased' do
      feature_layer.output_to_db
      expect(conn.exec(described_class::TABLES).map { |db| db['table_name'] }).to include '_lamp_posts'
    ensure
      clean_db_and_tmp_dir
    end

    it 'writes a set layers to PostgresSQL database tables for a group layer' do
      group_layer.output_to_db
      expect(conn.exec(described_class::TABLES).map { |tup| tup['table_name'] }.sort).to eq tables
    ensure
      clean_db_and_tmp_dir
    end

    it 'adds a suffix "_" to the table name if it is non-unique' do
      conn.exec('CREATE TABLE _lamp_posts (d date);')
      feature_layer.output_to_db # lamp_posts
      expect(table_names).to eq %w[_lamp_posts _lamp_posts_]
    ensure
      clean_db_and_tmp_dir
    end
  end

  describe '#geo' do
    it 'returns the esri geometry type from a JSON file' do
      expect(feature_layer.send(:geo)).to eq 'esriGeometryPoint'
    end
  end

  describe '#pg_geom' do
    it 'returns the PostGIS geometry type from a JSON file' do
      expect(feature_layer.send(:pg_geom)).to eq 'POINT'
    end

    it 'raises "Unknown geom type: <esri geometry>" for an unknown type' do
      layer = feature_layer
      allow(layer).to receive(:geo).and_return('esriGeometryUnknown')
      e = "Unknown geom: 'esriGeometryUnknown' for layer Lamp_Posts"
      expect { layer.send(:pg_geom) }.to raise_error e
    end
  end
end
