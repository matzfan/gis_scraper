# frozen_string_literal: true

require 'spec_helper'

describe GisScraper do
  it 'has a gem version number' do
    expect(GisScraper::VERSION).not_to be_nil
  end
end
